﻿using System;
using System.Collections.Generic;
using System.Text;
using Lon.IO;
using Lon.Util;
using Lon.IO.Ports;
using System.IO;
using System.Threading;

namespace LonDevSimulator
{
    class Program
    {
        static void Main(string[] args)
        {
           // SimulatorsRun();
            
           // DataFileSendTest();
            ZPW2000ADataFileSendTest();
        }
        static CanDevSimulatorManager manager;
        private static void SimulatorsRun()
        {
            String cfgPath = PathHelper.GetApplicationPath() + "\\Config\\采集机配置.xml";
            manager = new CanDevSimulatorManager(cfgPath);
            ICanDev canPort = new NetCanCard();
            manager.LoadDefaultVal(PathHelper.GetApplicationPath() + "\\Config\\启动默认值.ini");
            //   manager.SaveCurrentVal(PathHelper.GetApplicationPath() + "\\Config\\启动默认值.ini");
            Thread optThread = new Thread(new ThreadStart(OptProc));
            optThread.IsBackground = true;
            optThread.Start();
            manager.CanPort = canPort;
            while (true)
            {
                DateTime now = DateTime.Now;
                manager.Run(ref now);
                
                Thread.Sleep(1000);
            }
         
        }
        static public void OptProc()
        {
            while (true)
            {
                Thread.Sleep(20000);
                manager.SetDigit(1, 7, 20, 1);
                Thread.Sleep(20000);
                manager.SetDigit(1, 7, 20, 0);
            }
        }
        private static void DataFileSendTest()
        {
            ICanDev dev = new NetCanCard();
            List<CanFrame> frames = ReadCanFrame(".\\data.txt");
            while (true)
            {
                for (int i = 0; i < frames.Count; i++)
                {
                    dev.WriteCanFrame(frames[i]);
                }
                Thread.Sleep(1000);
            }
        }

        private static void ZPW2000ADataFileSendTest()
        {
            SeverSocketSend dev = new SeverSocketSend(5556);
            byte[] data=ReadData(".\\data2000.txt");
            while (true)
            {
                dev.WriteData(data);
                Thread.Sleep(1000);
            }
        }

        private static byte[] ReadData(string fileName)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.Open))
            {
                TextReader tr = new StreamReader(fs);
                String line;
                line = tr.ReadLine();

                byte[] bytes = StringHelper.BinaryFromString(line);

                return bytes;
            }
        }

        private static List<CanFrame> ReadCanFrame(string fileName)
        {
            List<CanFrame> res = new List<CanFrame>();
            using(FileStream fs = new FileStream(fileName, FileMode.Open))
            {
                TextReader tr = new StreamReader(fs);
                String line;
                while((line=tr.ReadLine())!=null)
                {
                   byte[] bytes= StringHelper.BinaryFromString(line);
                   if (bytes.Length < 10) continue;
                   CanFrame cf = new CanFrame();
                   cf.CanId = bytes[0] << 3;
                   cf.CanId += bytes[1] >> 5;
                   int len = bytes[1] & 0x0f;

                   cf.Buf=new byte[len];
                   for(int i=0;i<len;i++)
                   {
                       cf.Buf[i] = bytes[i + 2];
                   }
                   res.Add(cf);
                }
            }
            return res;


        }
    }
}
