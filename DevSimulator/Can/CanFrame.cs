﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO.Ports
{
   public  class CanFrame
    {
        public DateTime RecTime;
        public CanDataDir Dir = CanDataDir.ToHost;//是否下发数据
        public bool IsExFrame = false;//是否扩展帧
        public int CanId=0;
        public byte[] Buf = null;//数据缓存区
    }
}
