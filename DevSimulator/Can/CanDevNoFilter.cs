﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO.Ports
{
    public class CanDevNoFilter : IFilter<CanFrame>
    {
        private CanDataDir dir;
        private int devNo=-1;
        public CanDevNoFilter(CanDataDir dir)
        {
            devNo = -1;
            this.dir = dir;
        }
        public CanDevNoFilter(int devNo,CanDataDir dir)
        {
            this.devNo = devNo;
            this.dir = dir;

        }
        public CanDevNoFilter(int devNo):this(devNo,CanDataDir.ToHost)
        {
          
        }
        public bool Comp(CanFrame package)
        {
            if (package.Dir != this.dir) return false;
            if (devNo == -1) return true;
            if (devNo == ((package.CanId >> 3) & 0xff)) return true;
            return false;
        }

    }

    public class CanTJWXDevNoFilter : IFilter<CanFrame>
    {
        private CanDataDir dir;
        private int devNo = -1;
        public CanTJWXDevNoFilter(CanDataDir dir)
        {
            devNo = -1;
            this.dir = dir;
        }
        public CanTJWXDevNoFilter(int devNo, CanDataDir dir)
        {
            this.devNo = devNo;
            this.dir = dir;

        }
        public CanTJWXDevNoFilter(int devNo)
            : this(devNo, CanDataDir.ToHost)
        {

        }
        public bool Comp(CanFrame package)
        {
            if (package.Dir != this.dir) return false;
            if(this.dir==CanDataDir.ToHost)
            {
                if((package.CanId&0x400)==0) return false;
            }  
            if (devNo == -1) return true;
            if (((package.CanId >> 3) & 0x1f) == devNo) return true;
          
        
            return false;
        }

    }
}
