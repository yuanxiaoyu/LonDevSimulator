﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.IO;
using System.Diagnostics;

namespace Lon.IO.Ports
{
    public  class NetCanCard:ICanDev
    {

        Thread runThread;
        Thread txThread;

        IPEndPoint ipe = null;
        Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        List<NetCanCardClient> netCanCardClients = new List<NetCanCardClient>();
        Socket clientSocket=null;
    
        public NetCanCard()
        {
            Start(); 
        }

        public bool Start()
        {

            runThread = new Thread(new ThreadStart(RxProc));
            runThread.IsBackground = true;
            runThread.Start();
           
            return true;
        }

        public override CanFrame ReadFrame()
        {
            return null;
        }

        public override void WriteCanFrame(CanFrame cf)
        {
            for (int i = 0; i<this.netCanCardClients.Count; i++)
            {
                this.netCanCardClients[i].WriteCanFrame(cf);
            }
           
        }
        protected void RxProc()
        {

            try
            {
                IPEndPoint ipe
                  = new IPEndPoint(IPAddress.Any, 8685);
                s.Bind(ipe);
                s.Listen(100);

                while (true)
                {
                    try
                    {
                        clientSocket = s.Accept();
                        NetCanCardClient client = new NetCanCardClient(clientSocket);
                        for (int i = this.netCanCardClients.Count-1; i >=0; i--)
                        {
                            if(this.netCanCardClients[i].IsRunning)
                            {
                                continue;
                            }
                            this.netCanCardClients.RemoveAt(i);
                            Console.WriteLine("OneConnect Is Remove");
                        }
                        this.netCanCardClients.Add(client);
                        Console.WriteLine("OneConnect Is Add");
                    }
                    catch
                    {
                        Trace.WriteLine("catch");
                    }
                }
            }
            catch (System.Exception ex)
            {

            }



        }
       
         /// <summary>
        /// socket接收处理
        /// </summary>
        private void SocketRecive()
        {
          
            while (true)
            {
                byte[] buf = new byte[6000];
                int rxCount = clientSocket.Receive(buf);

                Thread.Sleep(1000);
                

            }
        }


        private byte[] BuildNetBuf(byte[] txBuf)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            bw.Write((byte)0x10);
            bw.Write((byte)0x02);
            for (int i = 0; i < txBuf.Length; i++)
            {
                if (txBuf[i] == 0x10)
                {
                    bw.Write((byte)0x10);
                    bw.Write((byte)0x10);
                }
                else
                {
                    bw.Write(txBuf[i]);
                }
            }
            bw.Write((byte)0x10);
            bw.Write((byte)0x03);
            return ms.ToArray();
        }

        private byte[] BuildDataBuf(CanFrame txFrame)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            byte portNo = 1;
            byte devNo = (byte)(txFrame.CanId >> 3);
            if (devNo > 20 && devNo < 32)
            {
                portNo = 2;
            }
            bw.Write((byte)0x01);
            bw.Write((byte)portNo);
            bw.Write(DateTime.Now.Ticks);
            bw.Write(txFrame.CanId);
            bw.Write((byte)txFrame.Buf.Length);
            bw.Write(txFrame.Buf);
            return ms.ToArray();
        }



    }
}
