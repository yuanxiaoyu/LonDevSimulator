﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Lon.UI;
using SourceGrid;


namespace Lon.Data.Station
{
    public partial class FormDevParamView:FormGroupGrid,ISave
    {
        List<Group> groups = new List<Group>();
        public FormDevParamView()
        {
            InitializeComponent();
            ColumnTitles.Clear();
            ColumnTitles.AddRange(new String[]{"通道号","通道类型","通道类型描述"
                ,"设置字节数","关联板号1","关联通道号1","关联板号1","关联通道号2","附加参数"});
        }
        private SampleDevParam10 showedDev = null;

        public bool Save()
        {
            if (showedDev == null) return false;
            else return SampleDevManager.SaveSampleDevParam(showedDev);
           
        }

        public bool SaveAs(string fileName)
        {
            if (showedDev == null) return false;
            else return SampleDevManager.SaveSampleDevParam(fileName,showedDev);
        }

        private void Form_Load(object sender, EventArgs e)
        {
            InitColumnHeader();
            grid.AutoSizeCells();
            grid.Columns.StretchToFit();
        }

        public void ShowParam(SampleDevParam10 dev)
        {
            SourceGrid.Cells.Editors.TextBoxNumeric editModel = new SourceGrid.Cells.Editors.TextBoxNumeric(typeof(int));
            DevAge.ComponentModel.Converter.NumberTypeConverter converter
                = new DevAge.ComponentModel.Converter.NumberTypeConverter(typeof(int));
            converter.NumberStyles = System.Globalization.NumberStyles.HexNumber;
            converter.Format = "X2";
            editModel.TypeConverter = converter;
           
            InitColumnHeader();
           
            groups.Clear();
            for(int i=0;i<dev.CardItems.Count;i++)
            {
               SampleDevCardParam10 card = dev.CardItems[i];
                Group group=AddGroup(card.CardNo.ToString()+"号板,板类型:"+card.CardType.ToString());
            
          

                SourceGrid.Cells.Editors.TextBox despModel = new SourceGrid.Cells.Editors.TextBoxNumeric(typeof(int));
                
                for (int j = 0; j < card.ChannelItems.Count; j++)
                {
                    int currRow = grid.Rows.Count;
                    AddGroupChildRow(currRow, group);
                    
                    SampleDevChannelParam10 channel = card.ChannelItems[j];
                    if (channel == null) continue;
                    grid[currRow, 0].Value=j;
                     //   = new SourceGrid.Cells.Cell(j);
                    grid[currRow, 0].Editor = editModel;
                    grid[currRow, 1].Value = channel.ChannelType;
                     // = new SourceGrid.Cells.Cell(channel.ChannelType);
                    grid[currRow, 1].Editor = editModel; 
                    
                    String desp = "";
                    if (channel.Attrib != null)
                    {
                        desp = channel.Attrib.Desp;
                    }
                     grid[currRow, 2].Value=desp;
                   
                    grid[currRow, 3].Value = channel.BytesCount;
                  
                    grid[currRow, 3].Editor = editModel;

                    grid[currRow, 4].Value=channel.RelatedCard1;
                    grid[currRow, 4].Editor = editModel;

                    grid[currRow, 5].Value= channel.RelatedChannel1;
                    grid[currRow, 5].Editor = editModel;
               
                    grid[currRow, 6].Value=channel.RelatedCard2;
                    
                    grid[currRow, 6].Editor = editModel;

                    grid[currRow, 7].Value=channel.RelatedChannel2;
             
                    grid[currRow, 7].Editor = editModel;

                    grid[currRow, 8].Value=channel.OtherParam;
                   
                    grid[currRow, 8].Editor = editModel;

                }
                grid.AutoSizeCells();
                grid.Columns.StretchToFit();
            }
        }

        public new void Show()
        {
            if (FormMain.GlobalPanel != null)
            {        
                  this.Show(FormMain.GlobalPanel);     
            }
            else
            {
                base.Show();
            }
        }

    }
}
