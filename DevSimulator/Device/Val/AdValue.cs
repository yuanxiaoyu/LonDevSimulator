﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class AnalogValue 
    {

        private float coefficient=1;

        public float Coefficient
        {
            get { return coefficient; }
            set { coefficient = value; }
        }

        private Int16 rawVal;

        public  Int16 MaxVal=30000;

        public  Int16 MinVal=-30000;

        public Int16 Val
        {
            get 
            { 
                return rawVal;
            }
            set 
            {

                if (value> MaxVal)
                {
                    rawVal = MaxVal;
                }
                else if (value < MinVal)
                {
                    rawVal = MinVal;
                }
                else
                {
                    rawVal = value;
                }

            }
        }


        public override string ToString()
        {
            return rawVal.ToString();
        }


    }
}
