﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class ChannelFSK:ChannelBase
    {


        public ChannelFSK(CardBase card)
            : base(card)
        {
            this.card = card;
            this.analogValues = new AnalogValue[3];
            InitAmpliVal();
            InitFreqVal();
            InitFmFreq();
        }

        private void InitAmpliVal()
        {
            AnalogValue ampli = new AnalogValue();
            this.analogValues[0] = ampli;
            ampli.MaxVal = 30000;
            ampli.MinVal = 0;
            ampli.Val = 1300;
        }

        private void InitFmFreq()
        {
            AnalogValue fmFreq = new AnalogValue();
            this.analogValues[2] = fmFreq;
            fmFreq.MaxVal = 300;
            fmFreq.MinVal = 70;
            fmFreq.Val = 114;
        }

        private void InitFreqVal()
        {
            AnalogValue freq = new AnalogValue();
            this.analogValues[1] = freq;
            freq.MaxVal = 26100;
            freq.MinVal = 16900;
            freq.Val = 17014;
        }


    }
}
