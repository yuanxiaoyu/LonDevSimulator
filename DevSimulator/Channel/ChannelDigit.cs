﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class ChannelDigit:ChannelBase 
    {
        protected CardBase card;
        protected AnalogValue[] analogValues;
     
        public AnalogValue[] Values
        {
            get { return analogValues; }
        }

        public ChannelDigit(CardBase card):base(card)
        {
            this.card = card;
            this.analogValues=new AnalogValue[0];
            this.digitValues=new DigitValue[2];
            for (int i = 0; i < digitValues.Length; i++)
            {
                this.digitValues[i] = new DigitValue();
            }
        }


    }
}
