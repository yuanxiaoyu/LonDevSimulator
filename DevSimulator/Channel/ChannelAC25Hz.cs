﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class ChannelAC25Hz:ChannelACWithFreqPhase
    {
      

        public ChannelAC25Hz(CardBase card):base(card)
        {
          
            this.analogValues[0].Coefficient = 40.0f / 800;
            this.analogValues[0].Val = 24*800/40;
            this.analogValues[1].Val = 900;
            this.analogValues[2].Val = 250;
        }


    }
}
