﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class ChannelAC:ChannelBase
    {
       
        public ChannelAC(CardBase card)
            :base(card)
        {
            this.card = card;
            this.analogValues = new AnalogValue[1];
            AnalogValue ampli = new AnalogValue();
            this.analogValues[0] = ampli;
            ampli.MaxVal = 800;
            ampli.MinVal = 0;
            ampli.Val = 400;
            
        }


    }
}
