﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class ChannelDC : ChannelBase
    {
      

        public ChannelDC(CardBase card):base(card)
        {
            this.card = card;
            this.analogValues = new AnalogValue[1];
            AnalogValue ampli = new AnalogValue();
            this.analogValues[0] = ampli;
            ampli.MaxVal = 2500;
            ampli.MinVal = 0;
            ampli.Val = 1000;
            
        }


    }
}
