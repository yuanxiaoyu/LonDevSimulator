﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
   public enum CardTypeEnum
    {
        DigitSampleBoard=1,
        DigitControlBoard=2,
        AnalogSampleBoard=3,
        FSKSampleBoard=4,
        MetroAnalogBoard=5
        //ToDo:须对照协议完成剩余部分

    }
   /// <summary>
   /// 固件类型
   /// </summary>
   public enum FirmwareType
   {
       CPU板固件 = 0,
       开入板固件 = 1,
       开出板固件 = 2,
       模入板BIOS固件 = 3,
       模入板系统固件 = 4,
       移频板核心BIOS固件 = 5,
       移频板核心系统固件 = 6,
       移频板F28335BIOS固件 = 7,
       移频板F28335系统固件 = 8,
       地铁模入板BIOS固件 = 9,
       地铁模入板系统固件 = 10,
       高压不对称板BIOS固件 = 11,
       高压不对称板系统固件 = 12,
       高压不对称智能传感器固件 = 13,
       高压道岔表示智能传感器固件 = 14,
       移频智能传感器固件 = 15,
       低压直流智能传感器固件 = 16,
       II型交流道表智能传感器固件 = 17,
       II型直流道表智能传感器固件 = 18,
       II型轨道电路智能传感器固件 = 19,
       II型交流转辙机智能传感器固件 = 20,
       II型驼峰ZD7转辙机智能传感器固件 = 21,
       II型高压不对称智能传感器固件 = 22,
       II型电源屏智能传感器固件 = 23,
       II型半自闭智能传感器固件 = 24,
       II型站联电压智能传感器固件 = 25,
       II型直流转辙机智能传感器固件 = 26,
       II型站内电码化智能传感器固件 = 27,
       II型有绝缘移频发送智能传感器固件 = 28,
       II型有绝缘接收智能传感器固件 = 29,
       II型无绝缘发送智能传感器固件 = 30,
       II型无绝缘接收智能传感器固件 = 31
   }

   /// <summary>
   /// 升级方式
   /// </summary>
   public enum UpdateMode
   {
       CPU = 0,
       address = 1,            //按下面指定的板卡地址升级；
       parammeter = 2          //按配置参数升级；
   }

   /// <summary>
   /// 帧位置
   /// </summary>
   public enum FramePosition
   {
       Head = 0x20,
       Middle = 0x40,
       End = 0x60
   }

   //public enum ParamType
   //{
   //    rsvd,
   //    全部插卡参数,
   //    采集机固件版本号
   //}
}
