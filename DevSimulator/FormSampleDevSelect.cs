﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lon.UI;
using SourceGrid;

namespace Lon.Data.Station
{
    public partial class FormSampleDevSelect : DockContent
    {
       
        SampleDevManager devManager = SampleDevManager.GetInstace();
        #region grid显示方式定义 控制方式定义
        static SourceGrid.Cells.Views.Cell viewDev = new SourceGrid.Cells.Views.Cell();
        static Image sampleDevImg;// 使用静态防止反复加载
        static DevEditor devEditor = new DevEditor(typeof(SampleDevParam10));
         SourceGrid.Cells.Controllers.Button devSelectButton
                                 = new SourceGrid.Cells.Controllers.Button();
         SourceGrid.Cells.Controllers.Button funButton
                                 = new SourceGrid.Cells.Controllers.Button();
        static SourceGrid.Cells.Views.Button viewBtn = new SourceGrid.Cells.Views.Button();
        #endregion
        static int lastSelect = 0;
        static FormSampleDevSelect()
        {
            try
            { 
                sampleDevImg = Image.FromFile(".\\Pic\\SampleDev.Jpg");
                viewDev.TextAlignment = DevAge.Drawing.ContentAlignment.BottomCenter;
                viewDev.ImageAlignment = DevAge.Drawing.ContentAlignment.MiddleCenter;
                viewDev.ImageStretch = true;
                viewBtn.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleCenter;
             
                
            }
            catch (System.Exception ex)
            {
            	
            }
          
        }

        static void devSelectButton_Executed(object sender, EventArgs e)
        {
            CellContext context = (CellContext)sender;
            if (context.Position.Row == lastSelect) return;
            lastSelect = context.Position.Row;
            SampleDevParam10 dev=(SampleDevParam10)(context.Value);
            CmdManager.RunCmd("ParamFileView " + dev.DevNo.ToString());
        }

        public FormSampleDevSelect()
        {
            devSelectButton.Executed += new EventHandler(devSelectButton_Executed);
            InitializeComponent();
            
        }

        private void FormSampleDevSelect_Load(object sender, EventArgs e)
        {
            InitDevGrid();
            InitButtonGrid();
        }

        private void InitDevGrid()
        {
            devSelectGrid.Redim(devManager.devDict.Count, 1);
            devSelectGrid.AutoStretchColumnsToFitWidth = true;
            devSelectGrid.Selection.EnableMultiSelection = false;
            int currRow=0;
            for(int i=1;i<100;i++)
            {
                if (!devManager.devDict.ContainsKey(i)) continue;
                devSelectGrid[currRow, 0]
                    = new SourceGrid.Cells.Cell(devManager.devDict[i]);
                devSelectGrid[currRow, 0].Image = sampleDevImg;
                devSelectGrid[currRow, 0].View = viewDev;
                devSelectGrid[currRow, 0].Editor = devEditor;
                devSelectGrid[currRow, 0].AddController(devSelectButton);
                currRow++;
            }
            devSelectGrid.AutoSizeCells();
            devSelectGrid.Columns.StretchToFit();
        }

        private void InitButtonGrid()
        {
            
            btnGrid.ColumnsCount=1;
            btnGrid.AutoStretchColumnsToFitWidth = true;
            btnGrid.Selection.EnableMultiSelection = false;

            AddButton("设置采集机", "SetParam","进入调试状态,并设置采集机");
            AddButton("校验采集机", "VerifyParam", "进入调试状态,读取采集机参数");
            AddButton("退出调试状态", "RstDev","保存并退出调试状态");
           
           
        }

        private SourceGrid.Cells.Cell AddButton(String btnText, String cmd)
        {
            int currRow = btnGrid.Rows.Count;
            btnGrid.Rows.Insert(currRow);
            SourceGrid.Cells.Cell cell = new SourceGrid.Cells.Cell(btnText);
            cell.Tag = cmd;
            cell.View = viewBtn;
            cell.AddController(funButton);
            
            btnGrid[currRow, 0] = cell;
            btnGrid[currRow, 0].Row.Height = 40;
            return cell;
        }
        private SourceGrid.Cells.Cell AddButton(String btnText, String cmd,String toolTipText)
        { 
            SourceGrid.Cells.Cell cell = AddButton( btnText, cmd);
            cell.ToolTipText = toolTipText;
            return cell;
        }
        public new void Show()
        {
            if(FormMain.GlobalPanel!=null)
            {
                base.Show(FormMain.GlobalPanel);
            }
            else
            {
                base.Show();
            }
        }

        private void devSelectGrid_Paint(object sender, PaintEventArgs e)
        {

        }
        private class DevEditor : SourceGrid.Cells.Editors.EditorBase
        {
            public DevEditor(Type p_Type)
                : base(p_Type)
            { }
            public override string ValueToDisplayString(object p_Value)
            {
                SampleDevParam10 dev = p_Value as SampleDevParam10;
                if (dev != null)
                {
                    return String.Format("{0}号采集机", dev.DevNo);
                }
                return "";
            }
        }
    }
    
}
