﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.Data.DataProcess
{
    public class DataProcesserManager<T>:IDataProcesser<T>
    {
        Dictionary<T, IDataProcesser<T>> processerDict = new Dictionary<T, IDataProcesser<T>>();


        #region IDataProcesser 成员

        public virtual bool Process(DataFrame<T> frame)
        {
            if (frame == null) return false;
            if (!processerDict.ContainsKey(frame.FrameType)) return false;
            IDataProcesser<T> processer = processerDict[frame.FrameType];
            //  if (processer == null) return false; //AddProcesser函数一定不会为空
            return processer.Process(frame);
        }

        #endregion

        public bool AddProcesser(IDataProcesser<T> processer,T frameType)
        {
            if (processer == null) return false;
            if (processerDict.ContainsKey(frameType)) return false;
            this.processerDict.Add(frameType, processer);
            return true;
        }
    }
}
