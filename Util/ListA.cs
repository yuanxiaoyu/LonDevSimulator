﻿using System;
using System.Collections.Generic;
using System.Text;
using Lon.Collect;


namespace Lon.Collect
{
    public class ListA<T> : List<T>, ICollectionA
    {
        private string createrA;

        public string CreaterA
        {
            get { return createrA; }
            set { createrA = value; }
        }
      

        public int CurCountA
        {
            get
            {
                int count = base.Count;
                return count;
            }
        }

        private int maxCountA;

        public int MaxCountA
        {
            get { return maxCountA; }
            set { maxCountA = value; }
        }


        private string nameA;

        public string NameA
        {
            get { return nameA; }
            set { nameA = value; }
        }


        private string typeA;

        public string TypeA
        {
            get { return typeA; }
            set { typeA = value; }
        }

        public ListA(int maxCount, string varName)
            : this(maxCount, varName, "未声明")
        { 
        }

        public ListA(int maxCount, string varName,string creater)
        {
            this.MaxCountA = maxCount;
           
            this.CreaterA = creater;
            this.NameA = varName;
            this.TypeA = "List";
            CollectionManager.Add(this);
        }

        public new void Add(T item)
        {
            bool count;
            base.Add(item);
            if (!CollectionManager.CanAlarm || this.MaxCountA <= 0)
            {
                count = true;
            }
            else
            {
                count = base.Count < this.MaxCountA;
            }
            bool flag = count;
            if (!flag)
            {
            }
        }

        public string GetMessage()
        {
            string str;
            bool count;
            if (!CollectionManager.CanAlarm || this.MaxCountA <= 0)
            {
                count = true;
            }
            else
            {
                count = base.Count < this.MaxCountA;
            }
            bool flag = count;
            if (flag)
            {
                str = null;
            }
            else
            {
                str = string.Concat(this.CreaterA, " = ", this.CurCountA);
            }
            return str;
        }
    }
}
