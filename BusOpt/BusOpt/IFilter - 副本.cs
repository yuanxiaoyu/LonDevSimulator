﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO.Ports
{
    public interface IFilter<T>
    {
        bool Comp(T val);
    }
}
