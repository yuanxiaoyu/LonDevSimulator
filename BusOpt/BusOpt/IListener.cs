﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Lon.IO.Ports
{
    public interface IListener<T>
    {
         void Write(T val); 
         bool Read(ref T val,int timeOut);
         bool Read(ref T val);
         AutoResetEvent RxEvent { get; }
         IFilter<T> RxFliter { get; set; }
         int BufLen { get; set; }
         void ClrBuffer();
    }
}
